from rest_framework import serializers
from django.utils import timezone

from app.accounts.models import User


ROLES = ['Vendor', 'User', 'Admin', 'Sales', 'Super Admin']


class UserSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField()

    def get_role(self, obj):
        return ROLES[obj.role.name - 1]

    class Meta:
        model = User
        fields = ('id', 'name', 'role', 'mobile', 'is_active')
