import logging
from datetime import datetime
from django.db import models
from django.utils import timezone
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib import admin
from django.conf import settings


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


class UserManager(BaseUserManager):

    def _create_user(self, name,email, password, **extra_fields):
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(name=name, email=email,
                          is_active=False, last_login=now,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.is_active = True
        user.save(using=self._db)
        return user

    def create_user(self, name=None, mobile=None, password=None, **extra_fields):
        return self._create_user(name, mobile, password, **extra_fields)

    def create_staff_user(self, mobile=None, password=None, **extra_fields):
        user = self._create_user(
            name, mobile, password, None, True, False, **extra_fields)
        user.is_staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, name, email, password, **extra_fields):
        user = self._create_user(name, email,
                                  password, **extra_fields)
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user

    def create_super_admin(self, mobile, password, name, **extra_fields):
        user = self._create_user(
            mobile, password, False, False, **extra_fields)
        user.is_super_admin = True
        user.first_name = name
        user.save(using=self._db)
        return user

    def deactivate(self):
        self.is_active = False
        self.save(update_fields=['is_active'])

    def get_obj_email(self, email):
        if User.email == email:
            return user
        else:
            return 0


class Role(models.Model):
    '''
    The Role entries are managed by the system,
    automatically created via a Django data migration.
    '''

    ADMIN=1
    USER=2

    ROLE_CHOICES=(
        (ADMIN, 'admin'),
        (USER, 'user'),
    )

    name=models.PositiveSmallIntegerField(
        choices=ROLE_CHOICES, primary_key=True)

    def __str__(self):
        return self.get_id_display()

    class Meta:
        db_table="role"


class User(AbstractBaseUser, PermissionsMixin):
    name=models.CharField(_('name'), max_length=160, blank=False, null=False)
    email = models.EmailField(_('email address'), max_length=255, unique=True)
    is_active=models.BooleanField(default=True)
    is_staff=models.BooleanField(default=False)
    role=models.ForeignKey(
        Role, on_delete=models.SET_NULL, null=True, unique=False)
    date_joined=models.DateTimeField(_('date joined'), default=timezone.now)
    pass_reset=models.BooleanField(default=False)
    reset_code=models.CharField(max_length=100, null=True, blank=False)
    otp=models.CharField(max_length=4, null=True, blank=True)
    is_verified=models.BooleanField(default=False)

    objects=UserManager()

    USERNAME_FIELD='email'
    REQUIRED_FIELDS=['name']

    class Meta:
        db_table="user"
        # verbose_name = _('user')
        # verbose_name_plural = _('users')

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def email_user(self, subject, message, from_email=None):
        send_mail(subject, message, from_email, [self.email])

    def send_otp_to_user(self, subject, mobile_number, from_mobile=None):
        # send otp function call
        pass

    def get_by_natural_key(self, username, pk):
        return self.get(**{self.model.USERNAME_FIELD: username, self.model.id: pk})

    def set_reset_code(self, code):
        log.info(self.reset_code)
        self.reset_code=code
        self.save()
        log.info(self.reset_code)
        return self.name

    def set_reset_code_null(self):
        self.reset_code=None
        self.save()

    def get_reset_code(self):
        return self.reset_code

    def set_pass_value(self, value):
        self.pass_reset=value
        self.save()

    def check_pass(self, password):
        if self.check_password(password):
            return 1
        else:
            return None


admin.site.register(User)
admin.site.register(Role)
